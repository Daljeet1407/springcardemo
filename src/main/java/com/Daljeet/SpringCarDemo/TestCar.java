package com.Daljeet.SpringCarDemo;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class TestCar {
    public static void main(String[] args) {
        // Using BeanFactory
        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("applicationContext.xml"));
        Car car = (Car) beanFactory.getBean("car");
        car.displayCarDetails();

        // Using ApplicationContext
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Car car1 = (Car) context.getBean("car");
        car1.displayCarDetails();
    }
}