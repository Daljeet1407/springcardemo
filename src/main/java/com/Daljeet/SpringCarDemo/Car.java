package com.Daljeet.SpringCarDemo;

public class Car {
    private String name;
    private String color;
    private String furlType;
    private int mileage;

    public Car() {
    }

    public Car(String name, String color, String furlType, int mileage) {
        this.name = name;
        this.color = color;
        this.furlType = furlType;
        this.mileage = mileage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFurlType() {
        return furlType;
    }

    public void setFurlType(String furlType) {
        this.furlType = furlType;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public void displayCarDetails() {
        System.out.println("Car Name: " + name);
        System.out.println("Car Color: " + color);
        System.out.println("Car Furl Type: " + furlType);
        System.out.println("Car Mileage: " + mileage);
    }
}